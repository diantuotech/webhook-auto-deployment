<?php
header("Content-Type: text/plain;charset=utf-8");
$hook = new WebHook();
$hook->start();
/**
 * Gitee Webhook
 * Auth: Leo
 * Blog: https://www.changliang.top
 */
class WebHook
{
	//签名密码
	const SECRET = 'xxxxxx';
	//日志文件路径
	const LOG_FILE = '/www/admin/wwwroot/log.txt';
	//本地目录
	const LOCAL_DIR = '/www/admin/wwwroot';

	//本地HOST
	const HOST = 'xxx.xxx.top';
	//是否记录日志
	const IS_LOG = true;
	const EMAIL = 'xxx@qq.com';
	const NAME = '流云大叔';

	private $file = '';
	private $host = '';
	private $token = '';
	private $timestrap = '';

	public function __construct()
	{
        try {
            $this->file = fopen(self::LOG_FILE, "a");
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }

	}

	public function start() {
	    if (!function_exists('exec')) {
	        $this->log('exec被禁用');
	        die;
        }
        $header = getallheaders();
        if (empty($header) || !is_array($header)) {
            $this->log('header不合法');
            die;
        }
        $this->host = trim($header['Host']);
        $this->token = trim($header['X-Gitee-Token']);
        $this->timestrap = trim($header['X-Gitee-Timestamp']);
        if ($this->host !== self::HOST || (time() - $this->timestrap) > 300) {
            $this->log('header参数不合法');
            die;
        }
        //验签
        $this->verify();

        if (file_exists(self::LOCAL_DIR . "/.git")) {
            exec("cd " . self::LOCAL_DIR . " && git pull origin master" . " 2>&1", $output, $exit);
            $this->log($output);
        }

    }

    /**
     * 记录日志
     */
    public function log($txt) {
        if (self::IS_LOG) {
            fputs($this->file, date("Y-m-d H:i:s") . $txt . "\n");
        }
        var_dump($txt);
    }

    /**
     * 验证签名
     */
	public function verify() {
	    $signStr = $this->timestrap . "\n" . self::SECRET;
        $hash = base64_encode(hash_hmac('sha256', $signStr, self::SECRET, true));
        if (!hash_equals($this->token, $hash)) {
            $this->log('签名验证失败，gitee签名：' . $this->token . '\n' . '本地签名：' . $hash . '\n'. '签名串：' . $signStr);
            die;
        }
    }
    public function __destruct() {
        fclose($this->file);
    }

}